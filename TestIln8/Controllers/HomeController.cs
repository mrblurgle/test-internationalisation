﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Globalization;
using System.Resources;
using System.Windows.Forms;
using System.Threading;

namespace TestIln8.Controllers
{

	[Internationalization]
	public class HomeController : Controller
	{

		public ActionResult Index ()
		{

			//Use @Resources.Resources.TestString in the view.
			//Ref: http://stackoverflow.com/a/11399849/201648

			var mvcName = typeof(Controller).Assembly.GetName ();
			var isMono = Type.GetType ("Mono.Runtime") != null;

			ViewData ["Version"] = mvcName.Version.Major + "." + mvcName.Version.Minor;
			ViewData ["Runtime"] = isMono ? "Mono" : ".NET";

			return View ();
		}

		public ActionResult SetToGerman() {
			HttpCookie iln8Cookie= new HttpCookie("iln8Cookie");
			iln8Cookie["Language"] = "de"; //Replace with the user's language
			iln8Cookie["Culture"] = "DE"; //Replace with the user's culture
			iln8Cookie.Expires = DateTime.Now.AddDays(90);
			HttpContext.Response.SetCookie(iln8Cookie);

			return View ();
		}
	}
}

